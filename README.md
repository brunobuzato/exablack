Train Router
========================
Author: Bruno Carlo Buzato

Technologies: CDI, JSF, JPA, EJB, JPA

What is it?
-----------

Projeto para solucionar problema proposto de cálculo de rotas de trem.

Architecture
-------------------

Foi pensado numa arquitetura básica e robusta com JEE6, usando para front-end o JSF 2, com o framework Primefaces 6, para back-end foi pensado na solução classica de EJB, para disponibilização dos serviços necessários, com JPA na cammada de persistencia.


Features
-------------------

A solução conta apenas com uma única página, nesta há tres abas:

1. Carregar caminhos: contém a funcionalidade de carregar o grafo para ser usado nas funcionalidades seguintes. O formato a ser usado é o mesmo proposto no documento  "1 - Coding Assignment (Programação).docx" enviado pela Monika Saviano, que é, por exemplo: "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7"
2. Cálculo de distância por caminho escolhido: provê a funcionalidade de calcular a distância do trajeto, conforme o usuário informa um novo nó (cidade). o calculo é atualiizado de acordo com a seleção.
3. Caminhos Possíveis: provê a funcionalidade de se informar o nó(cidade) de origem e destino, e calcular os camminhos possíveis de acordo com os parametros ou o mais curto.


System requirements
-------------------

Todo que é preciso para executar a aplicação é: Java 8.0 (Java SDK 1.8) ou mais novo e Maven 3.1 ou mais novo.

A aplicação será executada dentro de um servidor de aplicações JBoss Wildfly 8.1.0, que será disponibilizado pelo script de build.
 
Configure Maven
---------------

Se você necessitar configurar o maven, [Configure Maven](https://github.com/jboss-developer/jboss-developer-shared-resources/blob/master/guides/CONFIGURE_MAVEN.md) antes de executar.
 
Build, Test, Deploy and Run
-------------------------

1. Abra um terminal e navegue até o diretorio raiz do projeto, onde se localiza o arquivo pom.xml
2. Execute o comando:

        mvn wildfly:run

3. O maven irá compilar, testar, iniciar uma instância do servidor WildFly 8.1.0 na porta 8080 e realizar o deploy da aplicação.
4. Para parar o servidor, use o comando CRTL+C

Access the application 
---------------------

A aplicação estará disponível na URL: <http://localhost:8080/train-router/>.

Usuário: admin Senha: admin