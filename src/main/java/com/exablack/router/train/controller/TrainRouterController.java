package com.exablack.router.train.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.UploadedFile;

import com.exablack.router.train.model.Path;
import com.exablack.router.train.model.Town;
import com.exablack.router.train.service.Field;
import com.exablack.router.train.service.FilterTrip;
import com.exablack.router.train.service.Greatness;
import com.exablack.router.train.service.ServiceException;
import com.exablack.router.train.service.TownService;
import com.exablack.router.train.service.Trip;

/**
 * Menager Bean JSF provendo o controller para as paginas do site
 *
 * @author Bruno Carlo
 */
@ManagedBean(name = "trainRouter")
@ViewScoped
public class TrainRouterController {

	private List<Town> selectedTowns;

	private String typedGraph;

	private UploadedFile file;

	private Town originTown;

	private Town destinationTown;

	private String paths;

	private String tabIdSelected = "parameterFilter";

	private String tripFilterField;

	private String tripFilterGreatness;

	private String tripFilterValue;

	@EJB
	private TownService townService;

	public void upload() {
		if (this.file != null) {
			final String content = new String(this.file.getContents());

			try {
				this.townService.saveNewGraph(content);
				final FacesMessage message = new FacesMessage("Succesful", this.file.getFileName() + " is uploaded");
				FacesContext.getCurrentInstance().addMessage(null, message);
			} catch (final ServiceException e) {
				this.takeCareException(e);
			}
		}
	}

	public List<Town> getAllTowns() {
		try {
			final List<Town> towns = this.townService.getAllTowns();
			towns.sort((t1, t2) -> t1.getName().compareTo(t2.getName()));
			return towns;
		} catch (final ServiceException e) {
			this.takeCareException(e);
			return Collections.emptyList();
		}
	}

	private void takeCareException(final Exception e) {
		Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,
				e.fillInStackTrace().getMessage(), e);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getLocalizedMessage()));
	}

	public List<Town> selectedTown(final String query) {
		try {
			if (this.selectedTowns == null) {
				this.selectedTowns = new ArrayList<>();
			}

			final List<Town> towns = this.townService.getAllTowns();
			return towns.stream().filter((town) -> {
				return town.getName().startsWith(query) && !this.selectedTowns.contains(town);
			}).sorted((t1, t2) -> t1.getName().compareTo(t2.getName())).collect(Collectors.toList());
		} catch (final ServiceException e) {
			this.takeCareException(e);
			return Collections.emptyList();
		}
	}

	public void onSelectedTown(final SelectEvent event) {
		if ("path".equals(event.getComponent().getId())) {
			if (Town.class.isInstance(event.getObject())) {
				if (this.selectedTowns == null) {
					this.selectedTowns = new ArrayList<>();
				}
				this.selectedTowns.add((Town) event.getObject());
			}
		} else if ("inputOrigin".equals(event.getComponent().getId())) {
			this.originTown = (Town) event.getObject();
		} else if ("inputDestination".equals(event.getComponent().getId())) {
			this.destinationTown = (Town) event.getObject();
		}
	}

	public void onUnselectedTown(final UnselectEvent event) {
		if (Town.class.isInstance(event.getObject())) {
			if (this.selectedTowns != null) {
				this.selectedTowns.remove(event.getObject());
			}
		}
	}

	public void onTabChange(final TabChangeEvent event) {
		if (event.getTab() != null) {
			this.tabIdSelected = event.getTab().getId();
		}
	}

	public void calculePaths() {
		this.paths = "";
		if (this.originTown != null && this.destinationTown != null) {
			try {
				if ("minimumPath".equals(this.tabIdSelected)) {
					final List<Trip> trips = this.townService.getTrips(this.originTown, this.destinationTown,
							FilterTrip.SHORTEST_ROUTER);
					final Optional<Trip> trip = trips.stream()
							.sorted((t1, t2) -> t1.getTotalDistance().compareTo(t2.getTotalDistance())).findFirst();
					if (trip != null && trip.isPresent()) {
						this.paths = " A menor rota encontrada foi " + trip.get().getUnformatedPaths()
								+ " com a distancia de " + trip.get().getTotalDistance();
					} else {
						this.paths = "NO SUCH ROUTE";
					}
				} else if ("parameterFilter".equals(this.tabIdSelected)) {
					Field field = null;
					if (StringUtils.isNotBlank(this.tripFilterField)) {
						field = Field.valueOf(this.tripFilterField);
					}
					Greatness greatness = null;
					if (StringUtils.isNotBlank(this.tripFilterGreatness)) {
						greatness = Greatness.valueOf(this.tripFilterGreatness);
					}
					Double value = null;
					if (NumberUtils.isNumber(this.tripFilterValue)) {
						value = Double.valueOf(this.tripFilterValue);
					}
					if (field == null || greatness == null || value == null) {
						FacesContext.getCurrentInstance().addMessage(null,
								new FacesMessage("Para este calculo é necessario informar todos os parametros."));
					} else {
						final List<Trip> trips = this.townService.getTrips(this.originTown, this.destinationTown,
								FilterTrip.of(greatness, field, value));
						final StringBuffer sb = new StringBuffer();
						trips.forEach(trip -> sb.append("(").append(trip.getUnformatedPaths()).append(") "));
						this.paths = sb.toString();
					}
				}
			} catch (final ServiceException e) {
				this.takeCareException(e);
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Para este calculo é necessario selecionar a origem e o destino."));
		}
	}

	public String getPathCalculated() {
		String returnPath = "";
		if (CollectionUtils.isNotEmpty(this.selectedTowns) && this.selectedTowns.size() > 1) {
			try {
				final List<Path> paths = this.townService.getPath(this.selectedTowns);
				if (paths != null && !paths.isEmpty()) {
					returnPath = String.valueOf(paths.stream().mapToDouble(Path::getDistance).sum());
				} else if (this.selectedTowns.size() > 1) {
					returnPath = "NO SUCH ROUTE";
				}
			} catch (final ServiceException e) {
				this.takeCareException(e);
			}
		}
		return returnPath;
	}

	public void loadTypedGraph() {
		if (StringUtils.isNoneBlank(this.typedGraph)) {
			try {
				this.townService.saveNewGraph(this.typedGraph);
				final FacesMessage message = new FacesMessage("Grafo importado com sucesso.");
				FacesContext.getCurrentInstance().addMessage(null, message);
				this.typedGraph = null;
			} catch (final ServiceException e) {
				this.takeCareException(e);
			}
		}
	}

	public String getTypedGraph() {
		return this.typedGraph;
	}

	public void setTypedGraph(final String typedGraph) {
		this.typedGraph = typedGraph;
	}

	public Town getOriginTown() {
		return this.originTown;
	}

	public void setOriginTown(final Town originTown) {
		this.originTown = originTown;
	}

	public Town getDestinationTown() {
		return this.destinationTown;
	}

	public void setDestinationTown(final Town destinationTown) {
		this.destinationTown = destinationTown;
	}

	public String getPaths() {
		return this.paths;
	}

	public void setPaths(final String paths) {
		this.paths = paths;
	}

	public String getTripFilterField() {
		return this.tripFilterField;
	}

	public void setTripFilterField(final String tripFilterField) {
		this.tripFilterField = tripFilterField;
	}

	public String getTripFilterGreatness() {
		return this.tripFilterGreatness;
	}

	public void setTripFilterGreatness(final String tripFilterGreatness) {
		this.tripFilterGreatness = tripFilterGreatness;
	}

	public String getTripFilterValue() {
		return this.tripFilterValue;
	}

	public void setTripFilterValue(final String tripFilterValue) {
		this.tripFilterValue = tripFilterValue;
	}

	public UploadedFile getFile() {
		return this.file;
	}

	public void setFile(final UploadedFile file) {
		this.file = file;
	}

}
