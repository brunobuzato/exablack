package com.exablack.router.train.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.exablack.router.train.model.Town;

@FacesConverter("townConverter")
public class TownConverter implements Converter {

	@Override
	public Object getAsObject(final FacesContext fc, final UIComponent uic, final String value) {
		final Town t = new Town();
		t.setName(value);
		return t;
	}

	@Override
	public String getAsString(final FacesContext fc, final UIComponent uic, final Object object) {

		return object != null ? ((Town) object).getName() : null;
	}

}
