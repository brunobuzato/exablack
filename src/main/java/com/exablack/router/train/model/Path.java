package com.exablack.router.train.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Representacao do caminho entre duas cidades, esta pode ser complementada com
 * informacoes como duracao, preco, tipo de tereno
 *
 * @author Bruno Carlo
 *
 */
@Entity
@XmlRootElement
@Table(name = "path")
public class Path implements Serializable {

	private static final long serialVersionUID = 2646556211051677540L;

	@Id
	@GeneratedValue
	@Column(name = "path_id", nullable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "path_origin_town_id", referencedColumnName = "town_id", nullable = false)
	private Town origin;

	@ManyToOne
	@JoinColumn(name = "path_destination_town_id", referencedColumnName = "town_id", nullable = false)
	private Town destination;

	@Column(name = "path_destination", nullable = false)
	private Double distance;

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public Town getOrigin() {
		return this.origin;
	}

	public void setOrigin(final Town origin) {
		this.origin = origin;
	}

	public Town getDestination() {
		return this.destination;
	}

	public void setDestination(final Town destination) {
		this.destination = destination;
	}

	public Double getDistance() {
		return this.distance;
	}

	public void setDistance(final Double distance) {
		this.distance = distance;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.id).append(this.origin).append(this.destination).toHashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final Path other = (Path) obj;
		return new EqualsBuilder().append(this.id, other.id).append(this.origin, other.origin)
				.append(this.destination, other.destination).isEquals();
	}

	public String getRepresentPath() {
		return String.format("%s -> (%s) -> %s", this.origin.getName(), this.distance, this.destination.getName());
	}

	@Override
	public String toString() {
		return String.format("Path [id=%s, origin=%s, destination=%s, distance=%s]", this.id, this.origin,
				this.destination, this.distance);
	}

}
