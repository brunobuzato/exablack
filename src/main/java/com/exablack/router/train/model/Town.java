package com.exablack.router.train.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Representa uma cidade, pode ser complementada com informaações demograficas e
 * outras.
 *
 * @author Bruno Carlo
 */
@Entity
@XmlRootElement
@Table(name = "town", uniqueConstraints = @UniqueConstraint(columnNames = { "town_name" }))
public class Town implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -123243101044288359L;

	@Id
	@GeneratedValue
	@Column(name = "town_id", nullable = false)
	private Long id;

	@Column(name = "town_name", nullable = false, length = 200)
	private String name;

	@OneToMany
	@MapKey(name = "destination")
	@JoinColumn(name = "path_origin_town_id", referencedColumnName = "town_id", insertable = false, updatable = false)
	private Map<Town, Path> pathsTo;

	@OneToMany
	@MapKey(name = "origin")
	@JoinColumn(name = "path_destination_town_id", referencedColumnName = "town_id", insertable = false, updatable = false)
	private Map<Town, Path> pathsFrom;

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.id).append(this.name).toHashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final Town other = (Town) obj;
		return new EqualsBuilder().append(this.id, other.id).append(this.name, other.name).isEquals();
	}

	@Override
	public String toString() {
		return String.format("Town [id=%s, name=%s]", this.id, this.name);
	}

	public Map<Town, Path> getPathsTo() {
		return this.pathsTo;
	}

	public Map<Town, Path> getPathsFrom() {
		return this.pathsFrom;
	}

	public Path getPathTo(final Town destination) {
		return this.pathsTo != null ? this.pathsTo.get(destination) : null;
	}

	/**
	 * Adiciona um novo para desta cidade para outra
	 *
	 * @param destination
	 *            cidade destino
	 * @param path
	 *            caminho
	 */
	public void addPathTo(final Town destination, final Path path) {
		if (this.pathsTo == null) {
			this.pathsTo = new HashMap<>();
		}
		this.pathsTo.put(destination, path);
	}

	/**
	 * Adiciona um novo caminho que chega a esta cidade
	 *
	 * @param origin
	 *            cidade de origem
	 * @param path
	 *            caminho
	 */
	public void addPathFrom(final Town origin, final Path path) {
		if (this.pathsFrom == null) {
			this.pathsFrom = new HashMap<>();
		}
		this.pathsFrom.put(origin, path);
	}

}
