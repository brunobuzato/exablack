package com.exablack.router.train.service;

/**
 * Enum que representa a grandeza
 *
 * @author Bruno Carlo
 */
public enum Greatness {
	LESS_THAN, EXACTLY, MAXIMUM
}