package com.exablack.router.train.service;

/**
 * Campo do filtro
 *
 * @author Bruno Carlo
 */
public enum Field {
	JUMPS, DISTANCE
}