package com.exablack.router.train.service;

import javax.ejb.ApplicationException;
import javax.persistence.PersistenceException;

/**
 * Exception checada para os servicos
 *
 * @author Bruno Carlo
 *
 */
@ApplicationException(rollback = true)
public class ServiceException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public ServiceException() {
	}

	public ServiceException(final PersistenceException e) {
		super(e);
	}

	public ServiceException(final String msg) {
		super(msg);
	}

}
