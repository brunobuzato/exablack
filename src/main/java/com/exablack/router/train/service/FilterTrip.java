package com.exablack.router.train.service;

import java.util.Collections;
import java.util.List;

import com.exablack.router.train.model.Path;

/**
 * Filtro para a busca de viagens
 *
 * @author Bruno Carlo
 */
public class FilterTrip {

	public static final FilterTrip SHORTEST_ROUTER = new FilterTrip() {

		@Override
		public boolean isHitTarget(final Trip trip) {
			final List<Path> paths = trip.getPaths();
			return paths.get(0).getOrigin().equals(trip.getOrigin())
					&& paths.get(paths.size() - 1).getDestination().equals(trip.getDestination());
		}

		@Override
		public boolean isStillValid(final Trip trip) {

			return trip.getPaths().stream().filter(path -> Collections.frequency(trip.getPaths(), path) > 1)
					.count() == 0;

		}

	};

	private Greatness greatness;
	private Field field;
	private double value;

	private FilterTrip() {
	}

	public Greatness getGreatness() {
		return this.greatness;
	}

	public Field getField() {
		return this.field;
	}

	public double getValue() {
		return this.value;
	}

	/**
	 * Recupera um filtro apartir dos parametros
	 *
	 * @param greatness
	 *            do filtro
	 * @param field
	 *            do filtro
	 * @param value
	 *            do filtro
	 * @return o filtro construido
	 * @throws IllegalArgumentException
	 *             se greatness ou field for nulo
	 */
	public static FilterTrip of(final Greatness greatness, final Field field, final double value) {
		if (greatness == null) {
			throw new IllegalArgumentException("greatness cannot be null");
		}
		if (field == null) {
			throw new IllegalArgumentException("field cannot be null");
		}
		final FilterTrip ft = new FilterTrip();
		ft.field = field;
		ft.greatness = greatness;
		ft.value = value;
		return ft;
	}

	public boolean isStillValid(final Trip trip) {

		double comparation;
		switch (this.field) {
		case JUMPS:
			comparation = trip.getPaths().size();
			break;
		case DISTANCE:
			comparation = trip.getPaths().stream().mapToDouble(Path::getDistance).sum();
			break;
		default:
			comparation = Double.NaN;
			break;
		}
		return this.value >= comparation;
	}

	public boolean isHitTarget(final Trip trip) {
		if (this.isStillValid(trip)) {
			final List<Path> paths = trip.getPaths();
			if (paths.get(0).getOrigin().equals(trip.getOrigin())
					&& paths.get(paths.size() - 1).getDestination().equals(trip.getDestination())) {
				double comparation;
				switch (this.field) {
				case JUMPS:
					comparation = trip.getPaths().size();
					break;
				case DISTANCE:
					comparation = trip.getPaths().stream().mapToDouble(Path::getDistance).sum();
					break;
				default:
					comparation = Double.NaN;
					break;
				}

				switch (this.greatness) {
				case EXACTLY:
					return this.value == comparation;
				case LESS_THAN:
					return this.value > comparation;
				case MAXIMUM:
					return this.value >= comparation;
				}
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format("FilterTrip [greatness=%s, field=%s, value=%s]", this.greatness, this.field, this.value);
	}

}
