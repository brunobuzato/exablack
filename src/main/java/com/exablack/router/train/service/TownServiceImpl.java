package com.exablack.router.train.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.PersistenceException;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.exablack.router.train.dao.TownDao;
import com.exablack.router.train.model.Path;
import com.exablack.router.train.model.Town;

@Local
@Stateless
// @DeclareRoles({ "Manager" })
public class TownServiceImpl implements TownService {

	@EJB
	private TownDao dao;

	public void setDao(final TownDao dao) {
		this.dao = dao;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<Town> getAllTowns() throws ServiceException {
		try {
			return this.dao.getAllTowns();
		} catch (final PersistenceException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Path> getPath(final List<Town> townsToCalcule) throws ServiceException {
		if (townsToCalcule == null || townsToCalcule.size() < 2) {
			throw new ServiceException("So é possível calcular caminhos entre no minimo de duas cidades");
		}
		try {
			final List<Path> paths = new ArrayList<>();
			Town origin = this.dao.getTownByName(townsToCalcule.stream().findFirst().get().getName());
			for (int next = 1; next < townsToCalcule.size(); next++) {
				final Town destination = this.dao.getTownByName(townsToCalcule.get(next).getName());
				final Path path = origin.getPathTo(destination);
				if (path == null) {
					paths.clear();
					// no edge between origin and destination
					break;
				} else {
					paths.add(path);
					origin = destination;
				}
			}
			return paths;
		} catch (final PersistenceException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Trip> getTrips(final Town origin, final Town destination, final FilterTrip filter)
			throws ServiceException {
		if (origin == null) {
			throw new IllegalArgumentException("Origin cannot be null");
		}
		if (destination == null) {
			throw new IllegalArgumentException("Destination cannot be null");
		}
		if (filter == null) {
			throw new IllegalArgumentException("Filter cannot be null");
		}
		try {
			final Town myOrigin = this.dao.getTownByName(origin.getName());
			final Town myDestination = this.dao.getTownByName(destination.getName());
			final List<Future<Trip>> trips = Collections.synchronizedList(new ArrayList<>());
			for (final Path path : myOrigin.getPathsTo().values()) {
				final Trip trip = new Trip(myOrigin, myDestination);
				trip.addPath(path);
				if (MapUtils.isNotEmpty(path.getDestination().getPathsTo())) {
					trips.add(this.deepSearch(filter, trips, trip, path));
				}
			}

			final Stream<Trip> retorno = trips.stream().map(future -> {
				try {
					return future.get();
				} catch (InterruptedException | ExecutionException e) {
					Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
					return null;
				}
			});
			return retorno.filter((trip) -> {
				return trip != null && filter.isHitTarget(trip);
			}).collect(Collectors.toList());
		} catch (final PersistenceException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * Realiza a busca em profundidade da primeira combinacao de viagem que
	 * satisfaca o filtro
	 *
	 * @param filter
	 *            a ser usado
	 * @param trips
	 *            lista de viagens
	 * @param parent
	 *            viagem que gerou o caminho ate o momento
	 * @param path
	 *            'perna' atual da viagem
	 * @return viagem gerada ate o momento
	 */
	@Asynchronous
	private Future<Trip> deepSearch(final FilterTrip filter, final List<Future<Trip>> trips, final Trip parent,
			final Path path) {
		if (!filter.isStillValid(parent)) {
			return new AsyncResult<>(null);
		}

		final Town nextDestination = this.dao.getTownById(path.getDestination().getId());
		for (final Path nextPath : nextDestination.getPathsTo().values()) {
			final Trip trip = parent.createCopy();
			trip.addPath(nextPath);
			Logger.getLogger(this.getClass().getName()).finest("deepSearch: trip: " + trip);

			if (MapUtils.isNotEmpty(nextPath.getDestination().getPathsTo())) {
				trips.add(this.deepSearch(filter, trips, trip, nextPath));
			}
		}

		if (filter.isHitTarget(parent)) {
			return new AsyncResult<>(parent);
		}
		return new AsyncResult<>(null);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	// @RolesAllowed("Manager")
	public void saveNewGraph(final String content) throws ServiceException {
		try {
			this.dao.clearDB();
			final Map<String, Town> towns = new HashMap<>();
			for (final String text : StringUtils.split(content, ",")) {
				final char[] chars = text.trim().toCharArray();
				Town origin = towns.get(String.valueOf(chars[0]));
				if (origin == null) {
					origin = new Town();
					origin.setName(String.valueOf(chars[0]));
					origin = this.dao.save(origin);
					towns.put(origin.getName(), origin);
				}
				Town destination = towns.get(String.valueOf(chars[1]));
				if (destination == null) {
					destination = new Town();
					destination.setName(String.valueOf(chars[1]));
					destination = this.dao.save(destination);
					towns.put(destination.getName(), destination);
				}

				final Path path = new Path();
				path.setDestination(destination);
				path.setOrigin(origin);
				path.setDistance(Double.valueOf(text.trim().substring(2)));
				this.dao.save(path);
			}
		} catch (final PersistenceException pe) {
			throw new ServiceException(pe);
		}
	}

}
