package com.exablack.router.train.service;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.exablack.router.train.model.Path;
import com.exablack.router.train.model.Town;

/**
 * Representacao de uma viagem
 *
 * @author Bruno Carlo
 */
public class Trip implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -8175719446981433136L;
	private Town origin;
	private Town destination;
	private List<Path> paths;

	public Trip(final Town myOrigin, final Town myDestination) {
		this.destination = myDestination;
		this.origin = myOrigin;
	}

	public Town getOrigin() {
		return this.origin;
	}

	public void setOrigin(final Town origin) {
		this.origin = origin;
	}

	public Town getDestination() {
		return this.destination;
	}

	public void setDestination(final Town destination) {
		this.destination = destination;
	}

	public List<Path> getPaths() {
		return this.paths;
	}

	public void setPaths(final List<Path> paths) {
		this.paths = paths;
	}

	/**
	 * Adiciona um novo pedaco do caminho da viagem
	 *
	 * @param path
	 *            novo pedaco do caminho
	 */
	public void addPath(final Path path) {
		if (this.paths == null) {
			this.paths = new CopyOnWriteArrayList<>();
		}
		this.paths.add(path);
	}

	public Trip createCopy() {
		final Trip trip = new Trip(this.origin, this.destination);
		trip.paths = new CopyOnWriteArrayList<>(this.paths);
		return trip;
	}

	@Override
	public String toString() {
		return String.format("Trip [origin=%s, destination=%s, paths=%s]", this.origin, this.destination, this.paths);
	}

	public String getUnformatedPaths() {
		final StringBuffer sb = new StringBuffer();
		this.paths.forEach((path) -> sb.append(path.getOrigin().getName()));
		return sb.append(this.destination.getName()).toString();
	}

	public Double getTotalDistance() {
		return this.paths.stream().mapToDouble(Path::getDistance).sum();
	}

}
