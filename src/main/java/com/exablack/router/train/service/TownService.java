package com.exablack.router.train.service;

import java.util.List;

import com.exablack.router.train.model.Path;
import com.exablack.router.train.model.Town;

/**
 * Servico to towns, edges e paths
 *
 * @author Bruno Carlo
 */
public interface TownService {

	/**
	 * Get all towns
	 *
	 * @return list of towns
	 * @throws ServiceException
	 *             if any error
	 */
	List<Town> getAllTowns() throws ServiceException;

	/**
	 * Get paths using town's edge
	 *
	 * @param townsToCalcule
	 *            towns to calculete que path
	 * @return list of paths representing by towns's edge
	 * @throws ServiceException
	 *             if any error
	 */
	List<Path> getPath(final List<Town> townsToCalcule) throws ServiceException;

	/**
	 * Processa e salva um novo grafo das ligações entre as cidades
	 *
	 * @param content
	 *            string contendo o grafo
	 * @throws ServiceException
	 *             se algum dado incorreto ou erro ao persistir
	 */
	void saveNewGraph(String content) throws ServiceException;

	/**
	 * Busca viagens de acordo com o filtro
	 *
	 * @param origin
	 *            cidade de origem
	 * @param destination
	 *            cidade de destino
	 * @param filter
	 *            aa ser usado
	 * @return lista de viagens possiveis
	 * @throws ServiceException
	 *             se algum erro acontecer
	 */
	List<Trip> getTrips(Town origin, Town destination, FilterTrip filter) throws ServiceException;

}
