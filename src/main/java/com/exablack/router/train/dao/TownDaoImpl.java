package com.exablack.router.train.dao;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.exablack.router.train.model.Path;
import com.exablack.router.train.model.Town;
import com.exablack.router.train.model.Town_;

@Local
@Stateless
public class TownDaoImpl implements TownDao {

	private EntityManager entityManager;

	@PersistenceContext(unitName = "trainUnit")
	public void setEntityManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<Town> getAllTowns() {
		final CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<Town> criteria = cb.createQuery(Town.class);
		criteria.from(Town.class);
		return this.entityManager.createQuery(criteria).getResultList();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Town getTownByName(final String name) {
		final CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<Town> criteria = cb.createQuery(Town.class);
		final Root<Town> root = criteria.from(Town.class);
		criteria.where(cb.equal(root.get(Town_.name), name));
		return this.entityManager.createQuery(criteria).getSingleResult();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Town getTownById(final Long id) {
		return this.entityManager.find(Town.class, id);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public <T> T save(final T entity) {
		this.entityManager.persist(entity);
		return entity;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void clearDB() {
		final CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
		final CriteriaDelete<Path> pathCriteria = cb.createCriteriaDelete(Path.class);
		pathCriteria.from(Path.class);
		this.entityManager.createQuery(pathCriteria).executeUpdate();

		final CriteriaDelete<Town> townCriteria = cb.createCriteriaDelete(Town.class);
		townCriteria.from(Town.class);
		this.entityManager.createQuery(townCriteria).executeUpdate();
	}

}
