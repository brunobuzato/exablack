package com.exablack.router.train.dao;

import java.util.List;

import com.exablack.router.train.model.Path;
import com.exablack.router.train.model.Town;

/**
 * Interface de servico de acesso as entidades {@link Town} e {@link Path}
 *
 * @author Bruno Carlo
 *
 */
public interface TownDao {

	/**
	 * Recupera todas as cidades cadastradas
	 *
	 * @return lista de todas as cidades cadastradas
	 */
	List<Town> getAllTowns();

	/**
	 * Recupera uma cidade
	 *
	 * @param name
	 *            que se baseara a busca, eh obrigatorio
	 * @return a cidade ou nulo
	 */
	Town getTownByName(String name);

	/**
	 * Recupera uma cidade
	 *
	 * @param id
	 *            da cidade a ser recuperada, obrigatorio
	 * @return a cidade ou nulo
	 */
	Town getTownById(Long id);

	/**
	 * Armazena uma nova cidade
	 *
	 * @param town
	 *            a ser armazenada
	 * @return a cidade depois de armazenada
	 */
	<T> T save(final T entity);

	/**
	 * Limpa todas as cidades e caminhos cadastrados
	 */
	void clearDB();

}
