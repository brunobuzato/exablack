package com.exablack.router.train.service.arquillian;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.exablack.router.train.dao.TownDao;
import com.exablack.router.train.dao.TownDaoImpl;
import com.exablack.router.train.model.Path;
import com.exablack.router.train.model.Town;
import com.exablack.router.train.service.Field;
import com.exablack.router.train.service.FilterTrip;
import com.exablack.router.train.service.Greatness;
import com.exablack.router.train.service.ServiceException;
import com.exablack.router.train.service.TownService;
import com.exablack.router.train.service.TownServiceImpl;
import com.exablack.router.train.service.Trip;

@RunWith(Arquillian.class)
public class TownServiceImplArquillianTest {

	@Inject
	private TownService service;

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(WebArchive.class, "train.war")
				.addClasses(TownServiceImpl.class, TownService.class, TownDao.class, TownDaoImpl.class,
						ServiceException.class, FilterTrip.class, Trip.class, Greatness.class, Field.class)
				.addPackage("com.exablack.router.train.model").addPackages(true, "org.apache.commons.collections4")
				.addPackages(true, "org.apache.commons.lang3").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")

				// .addAsResource("META-INF/persistence.xml").addAsWebInfResource("WEB-INF/train-ds.xml");

				.addAsResource("META-INF/persistence.xml").addAsWebInfResource("test-ds.xml");
	}

	private Town town(final String name) {
		final Town t = new Town();
		t.setName(name);
		return t;
	}

	@Before
	public void setupClass() throws ServiceException {
		this.service.saveNewGraph("AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7");
	}

	@Test
	public void testRegister() throws Exception {
		List<Path> paths = this.service.getPath(Arrays.asList(this.town("A"), this.town("B"), this.town("C")));
		Assert.assertEquals(2, paths.size());
		double sum = paths.stream().mapToDouble(Path::getDistance).sum();
		Assert.assertEquals(9.0, sum, 0.01);

		paths = this.service.getPath(Arrays.asList(this.town("A"), this.town("D")));
		Assert.assertEquals(1, paths.size());
		sum = paths.stream().mapToDouble(Path::getDistance).sum();
		Assert.assertEquals(5.0, sum, 0.01);

		paths = this.service.getPath(Arrays.asList(this.town("A"), this.town("D"), this.town("C")));
		Assert.assertEquals(2, paths.size());
		sum = paths.stream().mapToDouble(Path::getDistance).sum();
		Assert.assertEquals(13.0, sum, 0.01);

		paths = this.service
				.getPath(Arrays.asList(this.town("A"), this.town("E"), this.town("B"), this.town("C"), this.town("D")));
		Assert.assertEquals(4, paths.size());
		sum = paths.stream().mapToDouble(Path::getDistance).sum();
		Assert.assertEquals(22.0, sum, 0.01);

		paths = this.service.getPath(Arrays.asList(this.town("A"), this.town("E"), this.town("D")));
		Assert.assertEquals(0, paths.size());
	}

	@Test
	public void testGetTrips() throws ServiceException {
		List<Trip> trips = this.service.getTrips(this.town("C"), this.town("C"),
				FilterTrip.of(Greatness.MAXIMUM, Field.JUMPS, 3));
		Assert.assertEquals(2, trips.size());
		trips.forEach((trip) -> {
			if (trip.getPaths().size() == 2) {
				Assert.assertEquals("CDC", trip.getUnformatedPaths());
			} else if (trip.getPaths().size() == 3) {
				Assert.assertEquals("CEBC", trip.getUnformatedPaths());
			}
		});

		trips = this.service.getTrips(this.town("A"), this.town("C"), FilterTrip.of(Greatness.EXACTLY, Field.JUMPS, 4));
		Assert.assertEquals(3, trips.size());
		trips.forEach((trip) -> {
			if (trip.getPaths().get(0).getDestination().getName().equals("B")) {
				Assert.assertEquals("ABCDC", trip.getUnformatedPaths());
			} else if (trip.getPaths().get(0).getDestination().getName().equals("D")
					&& trip.getPaths().get(1).getDestination().getName().equals("C")) {
				Assert.assertEquals("ADCDC", trip.getUnformatedPaths());
			} else if (trip.getPaths().get(0).getDestination().getName().equals("D")
					&& trip.getPaths().get(1).getDestination().getName().equals("E")) {
				Assert.assertEquals("ADEBC", trip.getUnformatedPaths());
			}
		});

		trips = this.service.getTrips(this.town("C"), this.town("C"),
				FilterTrip.of(Greatness.LESS_THAN, Field.DISTANCE, 30));
		Assert.assertEquals(7, trips.size());
		final List<String> pathEsperados = Arrays.asList("CDC", "CEBC", "CDEBC", "CEBCDC", "CDCEBC", "CEBCEBC",
				"CEBCEBCEBC");
		trips.forEach((trip) -> {
			Assert.assertTrue(pathEsperados.contains(trip.getUnformatedPaths()));
		});

		trips = this.service.getTrips(this.town("A"), this.town("C"), FilterTrip.SHORTEST_ROUTER);
		Optional<Trip> trip = trips.stream().sorted((t1, t2) -> t1.getTotalDistance().compareTo(t2.getTotalDistance()))
				.findFirst();
		Assert.assertTrue(trip.isPresent());
		Assert.assertEquals(9.0, trip.get().getTotalDistance(), 0.01);

		trips = this.service.getTrips(this.town("B"), this.town("B"), FilterTrip.SHORTEST_ROUTER);
		trip = trips.stream().sorted((t1, t2) -> t1.getTotalDistance().compareTo(t2.getTotalDistance())).findFirst();
		Assert.assertTrue(trip.isPresent());
		Assert.assertEquals(9.0, trip.get().getTotalDistance(), 0.01);

	}

}
