package com.exablack.router.train.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.tuple.Triple;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.exablack.router.train.dao.TownDao;
import com.exablack.router.train.model.Path;
import com.exablack.router.train.model.Town;

@RunWith(MockitoJUnitRunner.class)
public class TownServiceImplTest {

	private TownServiceImpl service;

	@Mock
	private TownDao townDao;

	@Before
	public void setup() {
		this.service = new TownServiceImpl();
		this.service.setDao(this.townDao);
	}

	private Town town(final int id, final String name) {
		final Town t = this.town(name);
		t.setId((long) id);
		return t;
	}

	private Town town(final String name) {
		final Town t = new Town();
		t.setName(name);
		return t;
	}

	public Map<String, Town> getGraph() {
		final Map<String, Town> towns = new HashMap<>();
		for (final String name : Arrays.asList("A", "B", "C", "D", "E")) {
			towns.put(name, this.town(towns.size(), name));
		}
		// AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7
		int count = 0;
		for (final Triple<String, String, Double> triple : Arrays.asList(Triple.of("A", "B", 5.0),
				Triple.of("B", "C", 4.0), Triple.of("C", "D", 8.0), Triple.of("C", "D", 8.0), Triple.of("D", "C", 8.0),
				Triple.of("D", "E", 6.0), Triple.of("A", "D", 5.0), Triple.of("C", "E", 2.0), Triple.of("E", "B", 3.0),
				Triple.of("A", "E", 7.0))) {
			final Path path = new Path();
			path.setId((long) count++);
			path.setDestination(towns.get(triple.getMiddle()));
			path.setOrigin(towns.get(triple.getLeft()));
			path.setDistance(triple.getRight());

			path.getOrigin().addPathTo(path.getDestination(), path);
			path.getDestination().addPathFrom(path.getOrigin(), path);
		}
		return towns;
	}

	public void configureMockReturn() {

		final Map<String, Town> towns = this.getGraph();
		Mockito.doReturn(towns.get("A")).when(this.townDao).getTownByName(Matchers.same("A"));
		Mockito.doReturn(towns.get("B")).when(this.townDao).getTownByName(Matchers.same("B"));
		Mockito.doReturn(towns.get("C")).when(this.townDao).getTownByName(Matchers.same("C"));
		Mockito.doReturn(towns.get("D")).when(this.townDao).getTownByName(Matchers.same("D"));
		Mockito.doReturn(towns.get("E")).when(this.townDao).getTownByName(Matchers.same("E"));

		Mockito.doReturn(towns.get("A")).when(this.townDao).getTownById(Matchers.same(Long.valueOf(0)));
		Mockito.doReturn(towns.get("B")).when(this.townDao).getTownById(Matchers.same(Long.valueOf(1)));
		Mockito.doReturn(towns.get("C")).when(this.townDao).getTownById(Matchers.same(Long.valueOf(2)));
		Mockito.doReturn(towns.get("D")).when(this.townDao).getTownById(Matchers.same(Long.valueOf(3)));
		Mockito.doReturn(towns.get("E")).when(this.townDao).getTownById(Matchers.same(Long.valueOf(4)));

	}

	@Test
	public void testGetPath() throws Exception {
		this.configureMockReturn();

		List<Path> paths = this.service.getPath(Arrays.asList(this.town("A"), this.town("B"), this.town("C")));
		Assert.assertEquals(2, paths.size());
		double sum = paths.stream().mapToDouble(Path::getDistance).sum();
		Assert.assertEquals(9.0, sum, 0.01);

		paths = this.service.getPath(Arrays.asList(this.town("A"), this.town("D")));
		Assert.assertEquals(1, paths.size());
		sum = paths.stream().mapToDouble(Path::getDistance).sum();
		Assert.assertEquals(5.0, sum, 0.01);

		paths = this.service.getPath(Arrays.asList(this.town("A"), this.town("D"), this.town("C")));
		Assert.assertEquals(2, paths.size());
		sum = paths.stream().mapToDouble(Path::getDistance).sum();
		Assert.assertEquals(13.0, sum, 0.01);

		paths = this.service
				.getPath(Arrays.asList(this.town("A"), this.town("E"), this.town("B"), this.town("C"), this.town("D")));
		Assert.assertEquals(4, paths.size());
		sum = paths.stream().mapToDouble(Path::getDistance).sum();
		Assert.assertEquals(22.0, sum, 0.01);

		paths = this.service.getPath(Arrays.asList(this.town("A"), this.town("E"), this.town("D")));
		Assert.assertEquals(0, paths.size());
	}

	@Test
	public void testGetTrips() throws ServiceException {
		this.configureMockReturn();

		List<Trip> trips = this.service.getTrips(this.town("C"), this.town("C"),
				FilterTrip.of(Greatness.MAXIMUM, Field.JUMPS, 3));
		Assert.assertEquals(2, trips.size());
		trips.forEach((trip) -> {
			if (trip.getPaths().size() == 2) {
				Assert.assertEquals("CDC", trip.getUnformatedPaths());
			} else if (trip.getPaths().size() == 3) {
				Assert.assertEquals("CEBC", trip.getUnformatedPaths());
			}
		});

		trips = this.service.getTrips(this.town("A"), this.town("C"), FilterTrip.of(Greatness.EXACTLY, Field.JUMPS, 4));
		Assert.assertEquals(3, trips.size());
		trips.forEach((trip) -> {
			if (trip.getPaths().get(0).getDestination().getName().equals("B")) {
				Assert.assertEquals("ABCDC", trip.getUnformatedPaths());
			} else if (trip.getPaths().get(0).getDestination().getName().equals("D")
					&& trip.getPaths().get(1).getDestination().getName().equals("C")) {
				Assert.assertEquals("ADCDC", trip.getUnformatedPaths());
			} else if (trip.getPaths().get(0).getDestination().getName().equals("D")
					&& trip.getPaths().get(1).getDestination().getName().equals("E")) {
				Assert.assertEquals("ADEBC", trip.getUnformatedPaths());
			}
		});

		trips = this.service.getTrips(this.town("C"), this.town("C"),
				FilterTrip.of(Greatness.LESS_THAN, Field.DISTANCE, 30));
		Assert.assertEquals(7, trips.size());
		final List<String> pathEsperados = Arrays.asList("CDC", "CEBC", "CDEBC", "CEBCDC", "CDCEBC", "CEBCEBC",
				"CEBCEBCEBC");
		trips.forEach((trip) -> {
			Assert.assertTrue(pathEsperados.contains(trip.getUnformatedPaths()));
		});

		trips = this.service.getTrips(this.town("A"), this.town("C"), FilterTrip.SHORTEST_ROUTER);
		Optional<Trip> trip = trips.stream().sorted((t1, t2) -> t1.getTotalDistance().compareTo(t2.getTotalDistance()))
				.findFirst();
		Assert.assertTrue(trip.isPresent());
		Assert.assertEquals(9.0, trip.get().getTotalDistance(), 0.01);

		trips = this.service.getTrips(this.town("B"), this.town("B"), FilterTrip.SHORTEST_ROUTER);
		trip = trips.stream().sorted((t1, t2) -> t1.getTotalDistance().compareTo(t2.getTotalDistance())).findFirst();
		Assert.assertTrue(trip.isPresent());
		Assert.assertEquals(9.0, trip.get().getTotalDistance(), 0.01);

	}
}
